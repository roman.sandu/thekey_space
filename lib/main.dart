import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jiffy/jiffy.dart';
import 'package:statusbar/statusbar.dart';
import 'package:thekey_space/bloc/tabs/tabs_bloc.dart';
import 'package:thekey_space/screens/main_screen.dart';

import 'bloc/calendar/calendar_bloc.dart';
import 'bloc/offices/offices_bloc.dart';
import 'core/constants.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    StatusBar.color(Colors.black);
    Jiffy.locale('ru');


  }

  @override
  Widget build(BuildContext context) {
    ThemeData buildThemeData() {
      return ThemeData(
        primaryColor: Colors.black,
        accentColor: Colors.black,
        appBarTheme: AppBarTheme(
          color: Colors.black,
        ),
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.black,
        ),
      );
    }

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.dark,
        statusBarBrightness: Brightness.dark,
      ),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: MultiBlocProvider(
          providers: [
            BlocProvider<TabsBloc>(
              create: (BuildContext context) => TabsBloc(),
            ),
            BlocProvider<OfficesBloc>(
              create: (BuildContext context) => OfficesBloc(),
            ),
            BlocProvider<CalendarBloc>(
              create: (BuildContext context) => CalendarBloc(),
            ),
          ],
          child: MaterialApp(
            title: 'the_key_space',
            theme: buildThemeData(),
            initialRoute: "/main",
            routes: {
              '/main': (context) => Main(),
            },
          ),
        ),
      ),
    );
  }
}
