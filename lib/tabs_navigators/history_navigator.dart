import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thekey_space/classes/navigation_context.dart';
import 'package:thekey_space/screens/history_screen.dart';

class HistoryTabNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey;

  HistoryTabNavigator({this.navigatorKey});

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context) {
    return {
      '/': (context) => History(),
    };
  }

  @override
  Widget build(BuildContext contextMain) {
    var routeBuilders = _routeBuilders(contextMain);
    return SafeArea(
      child: Navigator(
          key: navigatorKey,
          initialRoute: '/',
          onGenerateRoute: (routeSettings) {
            return MaterialPageRoute(
                settings: routeSettings,
                builder: (context) {
                  CustomNavigationContexts.historyContext = context;
                  return routeBuilders[routeSettings.name](context);
                });
          }),
    );
  }
}
