import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thekey_space/classes/navigation_context.dart';
import 'package:thekey_space/screens/calendar_screen.dart';
import 'package:thekey_space/screens/home_screen.dart';
import 'package:thekey_space/screens/workplace_screen.dart';

class HomeTabNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey;

  HomeTabNavigator({this.navigatorKey});

  Map<String, WidgetBuilder> _routeBuilders() {
    return {
      '/': (BuildContext context) => Home(),
      '/calendar': (BuildContext context) => Calendar(),
      '/workplace': (BuildContext context) => WorkPlace(),
    };
  }

  @override
  Widget build(BuildContext context) {
    var routeBuilders = _routeBuilders();

    return SafeArea(
      child: Navigator(
          key: navigatorKey,
          initialRoute: '/',
          onGenerateRoute: (routeSettings) {
            return MaterialPageRoute(
                settings: routeSettings,
                builder: (context) {
                  CustomNavigationContexts.homeContext = context;
                  return routeBuilders[routeSettings.name](context);
                });
          }),
    );
  }
}
