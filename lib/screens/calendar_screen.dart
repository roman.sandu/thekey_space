import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:thekey_space/bloc/calendar/calendar_bloc.dart';
import 'package:thekey_space/classes/user.dart';
import 'package:thekey_space/components/botton_component.dart';
import 'package:thekey_space/components/custom_button.dart';

class Calendar extends StatefulWidget {
  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> with TickerProviderStateMixin {
  AnimationController _animationController;
  CalendarController _calendarController;

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();

    _animationController = AnimationController(
      duration: const Duration(milliseconds: 400),
      vsync: this,
    );

    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  void _onDaySelected(DateTime day, List<User> events, List holidays) {
    BlocProvider.of<CalendarBloc>(context).add(
      ChangeSelectedReserveEvent(reserved: events, selectDay: day.toUtc()),
    );
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
  }

  void _onCalendarCreated(
      DateTime first, DateTime last, CalendarFormat format) {
  }

  Widget _buildTableCalendarWithBuilders() {
    return BlocBuilder<CalendarBloc, CalendarCurrentState>(
        builder: (context, state) {
      return TableCalendar(
        calendarController: _calendarController,
        events: state.calendarReserves[state.currentOfficeId],
        initialCalendarFormat: CalendarFormat.month,
        formatAnimation: FormatAnimation.slide,
        startingDayOfWeek: StartingDayOfWeek.sunday,
        availableGestures: AvailableGestures.all,
        availableCalendarFormats: const {
          CalendarFormat.month: '',
          CalendarFormat.week: '',
        },
        calendarStyle: CalendarStyle(
          selectedColor: Colors.black,
          todayColor: Colors.deepOrange[200],
          markersColor: Colors.yellow[700],
          outsideDaysVisible: false,
        ),
        daysOfWeekStyle: DaysOfWeekStyle(
          weekendStyle: TextStyle().copyWith(color: Colors.blue[600]),
        ),
        headerStyle: HeaderStyle(
          centerHeaderTitle: true,
          formatButtonVisible: false,
        ),
        builders: CalendarBuilders(
          selectedDayBuilder: (context, date, _) {
            return FadeTransition(
              opacity:
                  Tween(begin: 0.0, end: 1.0).animate(_animationController),
              child: Container(
                margin: EdgeInsets.all(4.0),
                padding: EdgeInsets.only(top: 5.0, left: 6.0),
                color: Colors.black,
                width: 100,
                height: 100,
                child: Text(
                  '${date.day}',
                  style:
                      TextStyle().copyWith(fontSize: 16.0, color: Colors.white),
                ),
              ),
            );
          },
          todayDayBuilder: (context, date, _) {
            return Container(
              margin: const EdgeInsets.all(4.0),
              padding: const EdgeInsets.only(top: 5.0, left: 6.0),
              color: Colors.blue[400],
              width: 100,
              height: 100,
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 16.0),
              ),
            );
          },
          markersBuilder: (context, date, events, holidays) {
            final children = <Widget>[];
            if (events.isNotEmpty) {
              children.add(
                Positioned(
                  right: 0,
                  top: 0,
                  child: _buildEventsMarker(date, events),
                ),
              );
            }

            return children;
          },
        ),
        onDaySelected: (date, events, holidays) {
          List<User> usersList = events.map((s) => s as User).toList();
          _onDaySelected(date, usersList, holidays);
          _animationController.forward(from: 0.0);
        },
        onVisibleDaysChanged: _onVisibleDaysChanged,
        onCalendarCreated: _onCalendarCreated,
      );
    });
  }

  Widget _buildEventsMarker(DateTime date, List events) {
    return Container(
      width: 15,
      height: 15,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.indigo,
        boxShadow: [
          BoxShadow(color: Colors.indigo, spreadRadius: 3),
        ],
      ),
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: 12.0,
          ),
        ),
      ),
    );
  }

  selectDate() {
    Navigator.of(context).pushNamed('/workplace');
  }

  Widget _buildEventList() {
    return BlocBuilder<CalendarBloc, CalendarCurrentState>(
        builder: (context, state) {
      final int reservedCount = state.selectedReserves?.length;
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomButton(
            onPress: reservedCount != 10 ? () => selectDate() : null,
            title:
                'Выбрать место (Доступно ${reservedCount == 0 ? 10 : 10 - reservedCount})',
          ),
          SizedBox(height: 8.0),
          Container(
            color: Colors.black.withOpacity(0.2),
            height: 2,
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: 10),
          ),
          SizedBox(height: 10.0),
          Container(
            margin: EdgeInsets.only(left: 10),
            child: Text(
              "Занято мест на дату ($reservedCount)",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
          ),
          SizedBox(height: 10.0),
          ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: state.selectedReserves
                .map((user) => Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: 0.8),
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      margin: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 4.0),
                      child: ListTile(
                        title: Text(user.name),
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://i.pravatar.cc/150?u=${user.id}"),
                        ),
                        subtitle: Text(user.email),
                      ),
                    ))
                .toList(),
          ),
        ],
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Выбрать дату'),
        ),
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                _buildTableCalendarWithBuilders(),
                Column(
                  children: [
                    _buildEventList(),
                  ],
                ),
                BottomPaddingContainer()
              ],
            ),
          ),
        ));
  }
}

