import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jiffy/jiffy.dart';
import 'package:thekey_space/bloc/calendar/calendar_bloc.dart';
import 'package:thekey_space/bloc/offices/offices_bloc.dart';
import 'package:thekey_space/classes/user.dart';
import 'package:thekey_space/components/botton_component.dart';
import 'package:thekey_space/components/custom_button.dart';
import 'package:thekey_space/screens/main_screen.dart';

class WorkPlace extends StatefulWidget {
  @override
  _WorkPlaceState createState() => _WorkPlaceState();
}

class _WorkPlaceState extends State<WorkPlace> {
  int currentPlace;

  successModal() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Бронирование создано!'),
            content: Container(
                height: 130,
                width: double.maxFinite,
                child: Column(
                  children: [
                    Divider(),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.of(navigatorKeys[TabName.HOME].currentContext)
                            .popUntil(
                                (route) => route.settings.name == "/calendar");
                      },
                      child: Text(
                        "Забронировать еще",
                        style: _navButton,
                      ),
                    ),
                    Divider(),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.of(navigatorKeys[TabName.HOME].currentContext)
                            .popUntil((route) => route.settings.name == "/");
                      },
                      child: Text(
                        "Завершить",
                        style: _navButton,
                      ),
                    ),
                  ],
                )),
          );
        });
  }

  reservePlace({List<User> selectedReserves}) {
    User mustBeCurrentUser = User(
        workplace: currentPlace,
        email: 'currentUser@gmail.com',
        name: 'currentUser');

    List<User> reserved = List.from(selectedReserves)..add(mustBeCurrentUser);

    BlocProvider.of<CalendarBloc>(context).add(
      UpdateSelectedReserveEvent(reserved: reserved),
    );

    successModal();
  }

  Widget setupAlertDialogContainer(BuildContext context, selectedReserves) {
    List placesReserved = selectedReserves.map((e) => e.workplace).toList();

    List<int> placesFree = [];

    for (var i = 0; i < 10; i++) {
      if (placesReserved.indexOf(i) == -1) {
        placesFree.add(i);
      }
    }

    return Container(
      height: MediaQuery.of(context).size.height / 3,
      width: double.maxFinite,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: placesFree.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              setState(() => currentPlace = placesFree[index]);
              Navigator.of(context).pop();
            },
            child: ListTile(
              title: Text('Место #${placesFree[index] + 1}'),
            ),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Бронирование места в офисе'),
        ),
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 10),
            child: BlocBuilder<CalendarBloc, CalendarCurrentState>(
                builder: (context, state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  BlocBuilder<OfficesBloc, OfficesCurrentState>(
                      builder: (context, state) {
                    return Text(state.currentOffice.title, style: _titleStyle);
                  }),
                  SizedBox(height: 10),
                  Text(
                      'Выберите день или промежуток для брони рабочего места в офисе (условия от заказчика)'),
                  SizedBox(height: 10),
                  Item(
                    title: 'Выбрать дату',
                    subTitle: Jiffy(state.selectDay).format('do MMMM yyyy'),
                    icon: Icons.arrow_right,
                    onPress: () => Navigator.of(context).pop(),
                  ),
                  SizedBox(height: 10),
                  Item(
                    title: 'Выбрать рабочее место',
                    subTitle: currentPlace != null
                        ? 'Место #${currentPlace + 1}'
                        : null,
                    icon: Icons.arrow_right,
                    onPress: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text('Выбрать рабочее место'),
                              content: setupAlertDialogContainer(
                                  context, state.selectedReserves),
                            );
                          });
                    },
                  ),
                  SizedBox(height: 20),
                  CustomButton(
                    title: 'Забронировать',
                    onPress: currentPlace != null
                        ? () => reservePlace(
                            selectedReserves: state.selectedReserves)
                        : null,
                  ),
                  BottomPaddingContainer()
                ],
              );
            }),
          ),
        ));
  }
}

class Item extends StatelessWidget {
  final String title;

  final String subTitle;

  final IconData icon;

  final Function onPress;

  Item({this.icon, this.title, this.subTitle, this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onPress?.call(),
      child: Container(
        height: 65,
        padding: EdgeInsets.symmetric(horizontal: 8),
        decoration: _itemDecor,
        child: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                        color: Colors.black.withOpacity(0.5),
                        fontSize: subTitle != null ? 14 : 18),
                  ),
                  subTitle != null
                      ? Text(
                          subTitle,
                          style: _titleStyle,
                        )
                      : Container(),
                ],
              ),
            ),
            Icon(icon, color: Colors.black.withOpacity(0.2)),
          ],
        ),
      ),
    );
  }
}

TextStyle _titleStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.bold,
);

const BoxShadow _shadow = BoxShadow(
    color: Colors.grey, spreadRadius: 2, blurRadius: 5, offset: Offset(0, 3));
const BoxDecoration _itemDecor = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.only(
      topLeft: Radius.circular(5),
      topRight: Radius.circular(5),
      bottomLeft: Radius.circular(5),
      bottomRight: Radius.circular(5)),
  boxShadow: [_shadow],
);
TextStyle _navButton = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.bold,
  color: Colors.deepPurple,
);
