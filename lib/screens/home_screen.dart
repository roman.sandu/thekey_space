import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jiffy/jiffy.dart';
import 'package:thekey_space/bloc/calendar/calendar_bloc.dart';
import 'package:thekey_space/bloc/offices/offices_bloc.dart';
import 'package:thekey_space/classes/office.dart';
import 'package:thekey_space/components/botton_component.dart';

class Home extends StatelessWidget {
  selectOffice({Office office, BuildContext context}) {
    BlocProvider.of<OfficesBloc>(context)
        .add(CurrentOfficeEvent(office: office));

    BlocProvider.of<CalendarBloc>(context)
        .add(UpdateCurrentOfficeIdEvent(currentOfficeId: office.id));

    Navigator.of(context).pushNamed('/calendar');
  }

  renderList(List<Office> offices) {
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: offices?.length ?? 0,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          Office office = offices[index];
          return GestureDetector(
            onTap: () => selectOffice(office: office, context: context),
            child: Container(
              margin: EdgeInsets.only(left: 8, right: 8, top: 10),
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: _itemDecor,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('${office.title}', style: _title),
                  SizedBox(height: 12),
                  Row(
                    children: [
                      Icon(Icons.perm_identity),
                      Text('${office.workspaceNumber} рабочих мест'),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(Icons.calendar_today_rounded),
                      Text(
                          '${Jiffy(office.availableStart).format('dd MMMM yyyy')} - ${Jiffy(office.availableEnd).format('dd MMMM yyyy')}'),
                    ],
                  ),
                  SizedBox(height: 12),
                  Text(
                      'Доступ: ${office.accessDesc} (${office.accessCount} чел)',
                      style: _title.copyWith(
                        fontWeight: FontWeight.w400,
                      )),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Бронирование места в офисе'),
        ),
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              BlocBuilder<OfficesBloc, OfficesCurrentState>(
                  builder: (context, state) {
                return renderList(state.offices);
              }),
              BottomPaddingContainer()
            ],
          ),
        ));
  }
}

const BoxShadow _shadow = BoxShadow(
    color: Colors.grey, spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3));
const BoxDecoration _itemDecor = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.only(
      topLeft: Radius.circular(10),
      topRight: Radius.circular(10),
      bottomLeft: Radius.circular(10),
      bottomRight: Radius.circular(10)),
  boxShadow: [_shadow],
);

TextStyle _title = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 16,
);
