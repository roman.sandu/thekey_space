import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:thekey_space/bloc/calendar/calendar_bloc.dart';
import 'package:thekey_space/bloc/offices/offices_bloc.dart';
import 'package:thekey_space/bloc/tabs/tabs_bloc.dart';
import 'package:thekey_space/classes/office.dart';
import 'package:thekey_space/classes/user.dart';
import 'package:thekey_space/components/bottom_navigation_bar.dart';
import 'package:thekey_space/core/constants.dart';
import 'package:thekey_space/tabs_navigators/history_navigator.dart';
import 'package:thekey_space/tabs_navigators/home_navigator.dart';

enum TabName { HOME, HISTORY }

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

Map<TabName, GlobalKey<NavigatorState>> navigatorKeys = {
  TabName.HOME: GlobalKey<NavigatorState>(),
  TabName.HISTORY: GlobalKey<NavigatorState>(),
};

class _MainState extends State<Main> {
  bool _keyboardShow = true;

  @override
  initState() {
    super.initState();

    final String office1 = uuid.v4();
    final String office2 = uuid.v4();

    List<Office> officesList = [
      Office(
        id: office1,
        title: 'Кузнецкий мост №303',
        workspaceNumber: 10,
        availableStart: DateTime.parse('2021-01-20 00:00:00.000'),
        availableEnd: DateTime.parse('2021-03-20 00:00:00.000'),
        accessDesc: 'Отдел маркетинга',
        accessCount: 22,
      ),
      Office(
        id: office2,
        title: 'Кузнецкий мост №201',
        workspaceNumber: 10,
        availableStart: DateTime.parse('2021-01-20 00:00:00.000'),
        availableEnd: DateTime.parse('2021-03-20 00:00:00.000'),
        accessDesc: 'Отдел маркетинга',
        accessCount: 36,
      ),
    ];

    BlocProvider.of<OfficesBloc>(context)
        .add(InitOfficeListEvent(offices: officesList));

    List<User> _selectedEvents = events[selectedDay.toUtc()] ?? [];

    BlocProvider.of<CalendarBloc>(context).add(
      InitEvent(
        reserved: _selectedEvents,
        calendar: {office1: events},
        selectDay: selectedDay.toUtc(),
      ),
    );
  }

  onTabIndex(int ind) {
    BlocProvider.of<TabsBloc>(context).add(
      TabChangeEvent(index: ind, tabName: TabName.values[ind]),
    );
  }

  renderTab(int index) {
    return CustomBottomNavigationBar(
      currentIndex: index,
      onTap: onTabIndex,
      items: [
        CustomBottomNavigationBarItem(
          activeIcon: Icon(Icons.home, color: Colors.black),
          icon: Icon(Icons.home, color: Colors.grey),
        ),
        CustomBottomNavigationBarItem(
          activeIcon: Icon(Icons.history, color: Colors.black),
          icon: Icon(Icons.history, color: Colors.grey),
        ),
      ],
    );
  }

  buildNavigationBar({bool notFinished = false}) {
    return BlocBuilder<TabsBloc, TabsState>(builder: (context, state) {
      if (state is TabsInitial) {
        return renderTab(state.index);
      } else if (state is SetTabsInd) {
        return renderTab(state.index);
      } else {
        return Container();
      }
    });
  }

  Widget renderNavigator(TabName tabName) {
    return Stack(
      children: <Widget>[
        Offstage(
          offstage: tabName != TabName.HOME,
          child: HomeTabNavigator(
            navigatorKey: navigatorKeys[TabName.HOME],
          ),
        ),
        Offstage(
          offstage: tabName != TabName.HISTORY,
          child: HistoryTabNavigator(
            navigatorKey: navigatorKeys[TabName.HISTORY],
          ),
        ),
        KeyboardVisibilityBuilder(builder: (context, _keyboardState) {
          return _keyboardShow
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: Theme(
                    data: ThemeData(canvasColor: Colors.black),
                    child: buildNavigationBar(),
                  ),
                )
              : Container();
        })
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      extendBody: true,
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      body: SafeArea(
        bottom: false,
        child: BlocBuilder<TabsBloc, TabsState>(builder: (context, state) {
          if (state is TabsInitial) {
            return renderNavigator(state.tabName);
          } else if (state is SetTabsInd) {
            return renderNavigator(state.tabName);
          } else {
            return Container();
          }
        }),
      ),
    );
  }
}
