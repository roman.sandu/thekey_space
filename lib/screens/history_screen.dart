import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jiffy/jiffy.dart';
import 'package:thekey_space/bloc/calendar/calendar_bloc.dart';
import 'package:thekey_space/bloc/offices/offices_bloc.dart';
import 'package:thekey_space/classes/history.dart';
import 'package:thekey_space/classes/office.dart';
import 'package:thekey_space/components/botton_component.dart';
import 'package:thekey_space/core/constants.dart';

class History extends StatelessWidget {
  remove({HistoryItem item, BuildContext context}) {
    BlocProvider.of<CalendarBloc>(context).add(RemoveReserveEvent(
      currentOfficeId: item.officeId,
      workplace: item.place,
      selectDay: item.date,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("История бронирований"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            BlocBuilder<CalendarBloc, CalendarCurrentState>(
                builder: (context, calendarState) {
              return BlocBuilder<OfficesBloc, OfficesCurrentState>(
                  builder: (context, officeState) {
                List list = [];
                calendarState.calendarReserves
                    .forEach((keyOffice, valueOffices) {
                  valueOffices.forEach((date, users) {
                    Office currentOffice = officeState.offices
                        .firstWhere((element) => element.id == keyOffice);
                    users.forEach((user) {
                      list.add(HistoryItem(
                        date: date,
                        place: user.workplace,
                        officeId: keyOffice,
                        officeTitle: currentOffice.title,
                        user: user.name,
                        id: uuid.v4(),
                      ));
                    });
                  });
                });

                return ListView.builder(
                  shrinkWrap: true,
                  itemCount: list.length,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    return Dismissible(
                      key: Key(list[index].id),
                      onDismissed: (direction) =>
                          remove(item: list[index], context: context),
                      background: Container(color: Colors.red),
                      child: Container(
                        height: 130,
                        margin: EdgeInsets.only(left: 12, right: 12, top: 10),
                        padding: EdgeInsets.only(left: 10, top: 4, bottom: 4),
                        decoration: _itemDecor,
                        width: double.maxFinite,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(list[index].officeTitle, style: _item),
                            Text(
                                '${Jiffy(list[index].date).format('dd MM yyyy')}',
                                style: _item),
                            Text('Место #${list[index].place}', style: _item),
                            Text('${list[index].user}', style: _item),
                          ],
                        ),
                      ),
                    );
                  },
                );
              });
            }),
            BottomPaddingContainer()
          ],
        ),
      ),
    );
  }
}

TextStyle _item = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.normal,
);

const BoxShadow _shadow = BoxShadow(
    color: Colors.grey, spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3));
const BoxDecoration _itemDecor = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.only(
      topLeft: Radius.circular(10),
      topRight: Radius.circular(10),
      bottomLeft: Radius.circular(10),
      bottomRight: Radius.circular(10)),
  boxShadow: [_shadow],
);

