part of 'offices_bloc.dart';

abstract class OfficesState extends Equatable {
  const OfficesState();
}

class OfficesCurrentState extends OfficesState {
  final List<Office> offices;
  final Office currentOffice;

  OfficesCurrentState({this.offices, this.currentOffice});

  copyWith({List<Office> offices, Office currentOffice}) => OfficesCurrentState(
      offices: offices ?? this.offices,
      currentOffice: currentOffice ?? this.currentOffice);

  @override
  List<Object> get props => [offices, currentOffice];
}
