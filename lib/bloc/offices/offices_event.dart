part of 'offices_bloc.dart';

abstract class OfficesEvent extends Equatable {
  const OfficesEvent();
}

class InitOfficeListEvent extends OfficesEvent {
  final List<Office> offices;

  InitOfficeListEvent({this.offices});

  @override
  List<Object> get props => [this.offices];
}

class CurrentOfficeEvent extends OfficesEvent {
  final Office office;

  CurrentOfficeEvent({this.office});

  @override
  List<Object> get props => [this.office];
}
