import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:thekey_space/classes/office.dart';

part 'offices_event.dart';

part 'offices_state.dart';

class OfficesBloc extends Bloc<OfficesEvent, OfficesCurrentState> {
  OfficesBloc() : super(OfficesCurrentState());

  @override
  Stream<OfficesCurrentState> mapEventToState(
    OfficesEvent event,
  ) async* {
    if (event is InitOfficeListEvent) {
      yield OfficesCurrentState(offices: event.offices);
    } else if (event is CurrentOfficeEvent) {
      yield state.copyWith(currentOffice: event.office);
    }
  }
}
