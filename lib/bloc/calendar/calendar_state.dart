part of 'calendar_bloc.dart';

abstract class CalendarState extends Equatable {
  const CalendarState();
}

class CalendarCurrentState extends CalendarState {
  final Map<String, Map<DateTime, List<User>>> calendarReserves;
  final List selectedReserves;
  final DateTime selectDay;
  final String currentOfficeId;

  CalendarCurrentState(
      {this.calendarReserves,
      this.selectedReserves,
      this.selectDay,
      this.currentOfficeId});

  copyWith({
    Map<String, Map<DateTime, List<User>>> calendarReserves,
    List<User> selectedReserves,
    DateTime selectDay,
    String currentOfficeId,
  }) =>
      CalendarCurrentState(
          calendarReserves: calendarReserves ?? this.calendarReserves,
          selectedReserves: selectedReserves ?? this.selectedReserves,
          selectDay: selectDay ?? this.selectDay,
          currentOfficeId: currentOfficeId ?? this.currentOfficeId);

  @override
  List<Object> get props =>
      [calendarReserves, selectedReserves, selectDay, currentOfficeId];
}
