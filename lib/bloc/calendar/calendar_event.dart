part of 'calendar_bloc.dart';

abstract class CalendarEvent extends Equatable {
  const CalendarEvent();
}

class ChangeSelectedReserveEvent extends CalendarEvent {
  final List<User> reserved;
  final DateTime selectDay;

  ChangeSelectedReserveEvent({this.reserved, this.selectDay});

  @override
  List<Object> get props => [this.reserved, this.selectDay];
}

class InitEvent extends CalendarEvent {
  final List<User> reserved;
  final Map<String, Map<DateTime, List<User>>> calendar;
  final DateTime selectDay;

  InitEvent({this.reserved, this.calendar, this.selectDay});

  @override
  List<Object> get props => [this.reserved, this.calendar, this.selectDay];
}

class UpdateSelectedReserveEvent extends CalendarEvent {
  final List<User> reserved;
  final DateTime selectDay;

  UpdateSelectedReserveEvent({this.reserved, this.selectDay});

  @override
  List<Object> get props => [this.reserved, this.selectDay];
}

class UpdateCurrentOfficeIdEvent extends CalendarEvent {
  final String currentOfficeId;

  UpdateCurrentOfficeIdEvent({this.currentOfficeId});

  @override
  List<Object> get props => [this.currentOfficeId];
}

class RemoveReserveEvent extends CalendarEvent {
  final String currentOfficeId;
  final DateTime selectDay;
  final int workplace;

  RemoveReserveEvent({this.currentOfficeId, this.selectDay, this.workplace});

  @override
  List<Object> get props =>
      [this.currentOfficeId, this.selectDay, this.workplace];
}
