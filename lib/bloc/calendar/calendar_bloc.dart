import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:thekey_space/classes/user.dart';
import 'package:thekey_space/core/constants.dart';

part 'calendar_event.dart';

part 'calendar_state.dart';

class CalendarBloc extends Bloc<CalendarEvent, CalendarCurrentState> {
  CalendarBloc()
      : super(CalendarCurrentState(
          calendarReserves: {},
          selectedReserves: [],
          selectDay: DateTime.now(),
        ));

  @override
  Stream<CalendarCurrentState> mapEventToState(
    CalendarEvent event,
  ) async* {
    if (event is ChangeSelectedReserveEvent) {
      yield state.copyWith(
        calendarReserves: state.calendarReserves,
        selectedReserves: event.reserved,
        selectDay: event.selectDay,
      );
    } else if (event is UpdateSelectedReserveEvent) {
      final DateTime selectDay = state.selectDay;
      final String currentOfficeId = state.currentOfficeId;
      Map<String, Map<DateTime, List<User>>> calendarReserves =
          state.calendarReserves;

      Map<DateTime, List<User>> currentOfficeCalendar =
          calendarReserves[currentOfficeId] != null
              ? calendarReserves[currentOfficeId]
              : {};

      if (currentOfficeCalendar.containsKey(selectDay)) {
        currentOfficeCalendar.update(selectDay, (value) => event.reserved);
      } else {
        currentOfficeCalendar[selectDay] = event.reserved;
      }

      if (calendarReserves.containsKey(currentOfficeId)) {
        calendarReserves.update(
            currentOfficeId, (value) => currentOfficeCalendar);
      } else {
        calendarReserves[currentOfficeId] = currentOfficeCalendar;
      }

      yield state.copyWith(
        calendarReserves: calendarReserves,
        selectedReserves: event.reserved,
      );
    } else if (event is InitEvent) {
      yield CalendarCurrentState(
        calendarReserves: event.calendar,
        selectedReserves: event.reserved,
        selectDay: event.selectDay,
      );
    } else if (event is UpdateCurrentOfficeIdEvent) {
      List<User> selectedReserves =
          state.calendarReserves[event.currentOfficeId] != null &&
                  state.calendarReserves[event.currentOfficeId]
                          [selectedDay.toUtc()] !=
                      null
              ? state.calendarReserves[event.currentOfficeId]
                  [selectedDay.toUtc()]
              : [];

      yield state.copyWith(
        currentOfficeId: event.currentOfficeId,
        selectedReserves: selectedReserves,
      );
    } else if (event is RemoveReserveEvent) {
      final DateTime selectDay = event.selectDay;
      final String currentOfficeId = event.currentOfficeId;

      Map<String, Map<DateTime, List<User>>> calendarReserves =
          state.calendarReserves;

      Map<DateTime, List<User>> currentOfficeCalendar =
          calendarReserves[currentOfficeId];
      List<User> reserves = List.from(currentOfficeCalendar[selectDay]);

      reserves.removeWhere((element) => element.workplace == event.workplace);

      currentOfficeCalendar.update(selectDay, (value) => reserves);

      calendarReserves.update(
          currentOfficeId, (value) => currentOfficeCalendar);

      yield state.copyWith(
        calendarReserves: calendarReserves,
        selectedReserves: calendarReserves[currentOfficeId][selectDay],
      );
    }
  }
}
