part of 'tabs_bloc.dart';

abstract class TabsState extends Equatable {
  const TabsState();

  List<Object> get props => [];
}

class TabsInitial extends TabsState {
  final int index;
  final TabName tabName;

  TabsInitial({this.index = 0, this.tabName = TabName.HOME});

  @override
  List<Object> get props => [index, tabName];
}

class SetTabsInd extends TabsState {
  final int index;
  final TabName tabName;

  SetTabsInd({this.index, this.tabName});

  @override
  List<Object> get props => [index, tabName];
}
