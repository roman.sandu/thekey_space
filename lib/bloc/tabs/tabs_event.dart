part of 'tabs_bloc.dart';

abstract class TabsEvent extends Equatable {}

class TabChangeEvent extends TabsEvent{
  final int index;
  final TabName tabName;
  TabChangeEvent({@required this.index,@required this.tabName});

  @override
  List<Object> get props => [index,tabName];
}

