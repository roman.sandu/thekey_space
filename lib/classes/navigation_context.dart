import 'package:flutter/cupertino.dart';

class CustomNavigationContexts {
  static BuildContext homeContext;
  static BuildContext historyContext;
}
