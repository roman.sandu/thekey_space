class Office {
  final String id;
  final String title;
  final int workspaceNumber;
  final DateTime availableStart;
  final DateTime availableEnd;
  final String accessDesc;
  final int accessCount;

  Office({
    this.id,
    this.title,
    this.workspaceNumber,
    this.availableStart,
    this.availableEnd,
    this.accessDesc,
    this.accessCount,
  });
}
