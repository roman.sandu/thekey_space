class User {
  final String id;
  final String name;
  final String email;
  final int workplace;

  User({this.email, this.name, this.id, this.workplace});
}
