class HistoryItem {
  final DateTime date;
  final int place;
  final String officeId;
  final String officeTitle;
  final String user;
  final String id;

  HistoryItem(
      {this.date,
      this.place,
      this.officeId,
      this.officeTitle,
      this.user,
      this.id});
}
