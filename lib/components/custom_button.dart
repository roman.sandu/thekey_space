import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String title;
  final Function onPress;

  CustomButton({
    this.title,
    @required this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10.0),
      child: FlatButton(
        height: 40,
        minWidth: double.infinity,
        child: Text(title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            )),
        onPressed: onPress,
        color: Colors.black,
        textColor: Colors.white,
        disabledColor: Colors.black.withOpacity(0.6),
        disabledTextColor: Colors.white,
      ),
    );
  }
}
