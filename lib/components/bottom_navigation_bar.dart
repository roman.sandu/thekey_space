import 'package:flutter/material.dart';

class CustomBottomNavigationBar extends StatelessWidget {
  final int currentIndex;

  final void Function(int index) onTap;
  final List<CustomBottomNavigationBarItem> items;

  CustomBottomNavigationBar({
    @required this.currentIndex,
    @required this.items,
    this.onTap,
  }) : assert(currentIndex != null);

  _buildItem(int index) {
    final item = items[index];
    return Expanded(
      flex: 1,
      child: GestureDetector(
        onTap: () => onTap?.call(index),
        child: Container(
          // color: Colors.transparent,
          height: 70,
          width: 70,
          // currentIndex == index
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(
                  width: currentIndex == index ? 6.0 : 3.0,
                  color: currentIndex == index
                      ? Colors.black
                      : Colors.black.withOpacity(0.3)),
            ),
            color: Colors.white,
          ),
          child: Stack(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    currentIndex == index ? item.activeIcon : item.icon,
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> listItems = List();
    items.asMap().forEach((key, value) => listItems.add(_buildItem(key)));
    return SafeArea(
      child: Container(
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: listItems,
        ),
      ),
    );
  }
}

class CustomBottomNavigationBarItem {
  final Widget activeIcon;
  final Widget icon;

  CustomBottomNavigationBarItem({
    this.activeIcon,
    this.icon,
  });
}
