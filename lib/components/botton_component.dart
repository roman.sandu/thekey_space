import 'package:flutter/material.dart';

class BottomPaddingContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(height: 70 + MediaQuery.of(context).padding.bottom);
  }
}
