import 'package:thekey_space/classes/user.dart';
import 'package:uuid/uuid.dart';

final DateTime now = DateTime.now();
final selectedDay = new DateTime(now.year, now.month, now.day, 15, 00, 00);

var uuid = Uuid();

List<User> userGenerate(int n) {
  List<User> list = [];

  for (var i = 0; i < n; i++) {
    String id = uuid.v4();
    list.add(User(
      name: 'Пользователь #$id',
      email: 'email$i@gmail.com',
      id: id,
      workplace: i,
    ));
  }
  return list;
}

Map<DateTime, List<User>> events = {
  selectedDay.subtract(Duration(days: 30)).toUtc(): userGenerate(8),
  selectedDay.subtract(Duration(days: 27)).toUtc(): userGenerate(1),
  selectedDay.subtract(Duration(days: 20)).toUtc(): userGenerate(4),
  selectedDay.subtract(Duration(days: 16)).toUtc(): userGenerate(1),
  selectedDay.subtract(Duration(days: 10)).toUtc(): userGenerate(4),
  selectedDay.subtract(Duration(days: 4)).toUtc(): userGenerate(3),
  selectedDay.subtract(Duration(days: 2)).toUtc(): userGenerate(2),
  selectedDay.toUtc(): userGenerate(4),
  selectedDay.add(Duration(days: 1)).toUtc(): userGenerate(4),
  selectedDay.add(Duration(days: 3)).toUtc(): userGenerate(3),
  selectedDay.add(Duration(days: 7)).toUtc(): userGenerate(3),
  selectedDay.add(Duration(days: 11)).toUtc(): userGenerate(2),
  selectedDay.add(Duration(days: 17)).toUtc(): userGenerate(4),
  selectedDay.add(Duration(days: 22)).toUtc(): userGenerate(1),
  selectedDay.add(Duration(days: 26)).toUtc(): userGenerate(10),
};
